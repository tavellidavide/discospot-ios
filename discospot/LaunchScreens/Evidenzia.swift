//
//  LaunchScreen.swift
//  discospot
//
//  Created by Davide Tavelli on 12/04/24.
//

import SwiftUI

struct Evidenzia: View {
    @State private var highlightWidth: CGFloat = 0 // La larghezza iniziale del layer di evidenziatore
    @State private var isActive = false
    
    let text = "discospot"

    var body: some View {
        ZStack {
             // Imposta lo sfondo nero per l'intero LaunchScreen
            Group {
                if isActive {
                    MainTabView()
                } else {
                    VStack {
                        Text(text)
                            .font(Font.custom("Druk Wide Bold", size: 45))
                            .foregroundStyle(.black)
                            .background(
                                GeometryReader { geometry in
                                    // Aggiungiamo il layer di evidenziatore sopra al testo
                                    Rectangle()
                                        .foregroundColor(.red)
                                        .frame(width: highlightWidth, height: 49)
                                        .animation(.easeInOut(duration: 4)) // Animazione del cambiamento di larghezza
                                }
                                .onAppear {
                                    // Calcoliamo la larghezza del layer di evidenziatore per ogni lettera
                                    for (index, _) in text.enumerated() {
                                        let width = CGFloat(index + 8) * 20 // Moltiplichiamo l'indice per una larghezza approssimativa per lettera
                                        DispatchQueue.main.asyncAfter(deadline: .now() + Double(index) * 0.1) {
                                            highlightWidth = width // Aggiorniamo la larghezza del layer con un ritardo per ottenere l'effetto lettera per lettera
                                            }
                                        }
                                    }
                                
                                
                            )

                        
                        ProgressView() // Aggiunge una rotella di caricamento
                            .progressViewStyle(CircularProgressViewStyle(tint: .white))
                            .scaleEffect(1) // Imposta la scala della rotella di caricamento
                            .offset(y: 100) // Sposta la rotella di caricamento verso il basso

                            
                        }
                    
                    
                    }

                }
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                    self.isActive = true
                }
            }
        }
        .statusBar(hidden: true) // Nasconde la barra di stato
    }
}

struct Evidenzia_Previews: PreviewProvider {
    static var previews: some View {
        Evidenzia()
    }
}
