//
//  Lampadina.swift
//  discospot
//
//  Created by Davide Tavelli on 17/04/24.
//

import SwiftUI

struct Lampadina: View {
    @State private var isOnAir = false

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 20)
                .frame(width: 100, height: 200)
                .foregroundColor(.black)

            Text("discospot")
                .font(.custom("Druk Wide Bold", size: 45)) // Imposta il font personalizzato qui
                .foregroundColor(.clear)
                .background(
                    ZStack {
                        LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0.2), Color.red.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
                            .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45))) // Imposta lo stesso font personalizzato qui

                        LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0), Color.red.opacity(0.5)]), startPoint: .top, endPoint: .bottom)
                            .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45))) // Imposta lo stesso font personalizzato qui
                            .offset(x: isOnAir ? 10 : -10) // Simulate flickering effect
                            .opacity(isOnAir ? 1 : 0)
                            .animation(Animation.linear(duration: 0.5).repeatForever(autoreverses: true))
                    }
                )
                .onAppear {
                    withAnimation {
                        self.isOnAir.toggle()
                    }
                }
        }
    }
}

struct Lampadina_Previews: PreviewProvider {
    static var previews: some View {
        Lampadina()
    }
}


