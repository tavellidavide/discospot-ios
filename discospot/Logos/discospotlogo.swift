//
//  discospotlogo.swift
//  discospot
//
//  Created by Davide Tavelli on 10/04/24.
//

import SwiftUI

struct discospotlogo: View {
    var body: some View {
        VStack(alignment: .leading){
            Text("discospot")
                .font(Font.custom("Druk Wide Bold", size: 45))
                .foregroundStyle(.black)
                .background(.red)
        }
    }
}

struct discospotlogo_Previews: PreviewProvider {
    static var previews: some View {
        discospotlogo()
    }
}
