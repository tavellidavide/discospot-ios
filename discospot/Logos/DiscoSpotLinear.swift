//
//  DiscoSpotLinear.swift
//  discospot
//
//  Created by Davide Tavelli on 17/04/24.
//

import SwiftUI

struct DiscoSpotLinear: View {
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0.2), Color.red.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
            .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45)))
    }
}

#Preview {
    DiscoSpotLinear()
}
