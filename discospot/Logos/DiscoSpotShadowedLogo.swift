//
//  DiscoSpotShadowedLogo.swift
//  discospot
//
//  Created by Davide Tavelli on 17/04/24.
//

import SwiftUI

struct DiscoSpotShadowedLogo: View {
    var body: some View {
        VStack(alignment: .leading){
            Text("discospot")
                .font(Font.custom("Druk Wide Bold", size: 45))
                .foregroundStyle(.black)
                .shadow(color: Color.red, radius: 1)
        }
        .padding(.top, 10)
    }
}

#Preview {
    DiscoSpotShadowedLogo()
}
