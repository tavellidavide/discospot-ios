//
//  discospotApp.swift
//  discospot
//
//  Created by Davide Tavelli on 09/04/24.
//

import SwiftUI
import Firebase

@main
struct discospotApp: App {
    init(){
        FirebaseApp.configure()
    }
    
    @State private var highlightWidth: CGFloat = 0 // La larghezza iniziale del layer di evidenziatore
    @State private var isActive = false
    @State private var isOnAir = false
    
    let text = "discospot"
    var body: some Scene {
        WindowGroup {
            ZStack {
                 // Imposta lo sfondo nero per l'intero LaunchScreen
                Group {
                    if isActive {
                        MainTabView()
                            .transition(.blurReplace)
                    } else {
                        VStack {
                            ZStack {
                                RoundedRectangle(cornerRadius: 20)
                                    .frame(width: 100, height: 200)
                                    .foregroundColor(.black)

                                Text("discospot")
                                    .font(.custom("Druk Wide Bold", size: 45)) // Imposta il font personalizzato qui
                                    .foregroundColor(.clear)
                                    .background(
                                        ZStack {
                                            LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0.2), Color.red.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
                                                .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45))) // Imposta lo stesso font personalizzato qui

                                            LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0), Color.red.opacity(0.5)]), startPoint: .top, endPoint: .bottom)
                                                .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45))) // Imposta lo stesso font personalizzato qui
                                                .offset(x: isOnAir ? 10 : -10) // Simulate flickering effect
                                                .opacity(isOnAir ? 1 : 0)
                                                .animation(Animation.linear(duration: 0.5).repeatForever(autoreverses: true))
                                        }
                                    )
                                    .onAppear {
                                        withAnimation {
                                            self.isOnAir.toggle()
                                        }
                                    }
                                ProgressView() // Aggiunge una rotella di caricamento
                                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
                                    .scaleEffect(1) // Imposta la scala della rotella di caricamento
                                    .offset(y: 100) // Sposta la rotella di caricamento verso il basso
                            }

                                
                            }
                        
                        
                        }

                    }
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                        self.isActive = true
                    }
                }
            }
        }
    }
}
