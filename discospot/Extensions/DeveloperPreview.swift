//
//  DeveloperPreview.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import Foundation

class DeveloperPreview{
    static let shared = DeveloperPreview()
    var listings: [Listing] = [
        .init(
            id: NSUUID().uuidString,
            ownerUid: NSUUID().uuidString,
            ownerName: "Tavo",
            ownerImageUrl: "tavo",
            DescriptionProfile: "Bella gnari!!",
            partecipanti: 10,
            priceOfTheEvent: 5,
            latitude: 25.7850,
            longitude: -80.1936,
            imageURLs: ["listing-1", "listing-2", "listing-3", "listing-4"],
            address: "Porcodio street",
            city: "Maderino",
            state: "Italia",
            title: "Tony Boy",
            rating: 3,
            features: [.atHouse, .superHost],
            amenities: [.wifi, .dj, .tv],
            type: .evento
        ),
        .init(
            id: NSUUID().uuidString,
            ownerUid: NSUUID().uuidString,
            ownerName: "GiammarcoBaroni",
            ownerImageUrl: "giammo",
            DescriptionProfile: "IG: giammarcobaroni",
            partecipanti: 10,
            priceOfTheEvent: 5,
            latitude: 25.7850,
            longitude: -80.1936,
            imageURLs: ["listing-3"],
            address: "Giammo's house",
            city: "Castiglione d/S italy",
            state: "Italia",
            title: "Festino",
            rating: 5,
            features: [.atHouse, .superHost],
            amenities: [.wifi, .dj, .tv],
            type: .raduno
        )]
}
