//
//  ContentView.swift
//  discospot
//
//  Created by Davide Tavelli on 09/04/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainTabView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
