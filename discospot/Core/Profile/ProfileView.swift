//
//  ProfileView.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import SwiftUI
import Firebase

struct ProfileView: View {
    @State private var username = ""
    @State private var email = ""
    @State private var password = ""
    @State private var userIsLoggedIn = false
    @State private var currentUser: String? = nil
    
    
    var body: some View {

        if userIsLoggedIn {
            // go somewhere
            loggedcontent
            
        } else {
            content
            
        }
    }
    
    var loggedcontent: some View {
        VStack {
            //Profile login view
            VStack(alignment: .leading, spacing: 32){
                
                VStack(alignment: .leading, spacing: 8){
                    Text("discospot:// \(currentUser ?? "Utente non trovato")")
                        .font(Font.custom("Druk Wide Bold", size: 40))
                        .foregroundStyle(.black)
                        .background(.red)

                    
                    Spacer()
                    
                    Button{
                        logout()
                    } label : {
                        Text("Logout")
                            .foregroundStyle(.white)
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .frame(width: 360, height: 48)
                            .background(.pink)
                            .clipShape(RoundedRectangle(cornerRadius: 8))
                    }
                        
                    
                }
                .font(.caption)
            }
            .onAppear {
                Auth.auth().addStateDidChangeListener { auth, user in
                    if user != nil {
                        userIsLoggedIn.toggle()
                    }
                }
            }
            
            //Profile option
            VStack(spacing:24) {
                ProfileOptionRowView(imageName: "gear", title: "Settings")
                
                ProfileOptionRowView(imageName: "gear", title: "Accessibility")
                
                ProfileOptionRowView(imageName: "questionmark.circle", title: "visit the help center")
            }
            .padding(.vertical)
        }
        .padding()
    }
    
    var content: some View {
        VStack {
            //Profile login view
            VStack(alignment: .leading, spacing: 32){
                
                VStack(alignment: .leading, spacing: 8){
                    Text("discospot:// spotter")
                        .font(Font.custom("Druk Wide Bold", size: 40))
                        .foregroundStyle(.black)
                        .background(.red)
                    
                    Text("Crea un account per unirti alle feste di tutto il mondo!")
                    
                    Spacer()
                    
                    VStack(spacing: 20){
                        
                        TextField("Username", text: $username)
                            .foregroundColor(.white)
                            .textFieldStyle(.plain)
                            .placeholder(when: username.isEmpty){
                                Text("Username")
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        
                        Rectangle()
                            .frame(width: 350, height: 1)
                            .foregroundColor(.gray)
                        
                        TextField("Email", text: $email)
                            .foregroundColor(.white)
                            .textFieldStyle(.plain)
                            .placeholder(when: email.isEmpty){
                                Text("Email")
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        
                        Rectangle()
                            .frame(width: 350, height: 1)
                            .foregroundColor(.gray)
                        
                        SecureField("Password", text: $password)
                            .foregroundColor(.white)
                            .textFieldStyle(.plain)
                            .placeholder(when: password.isEmpty){
                                Text("Password")
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        
                        Rectangle()
                            .frame(width: 350, height: 1)
                            .foregroundColor(.gray)
                    }
                }
                
                Button{
                    register()
                } label : {
                    Text("Registrati")
                        .foregroundStyle(.white)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .frame(width: 360, height: 48)
                        .background(.red)
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                }
                
                HStack{
                    Text("Sei già uno spotter?")
                    
                    Button{
                        login()
                    } label : {
                        Text("Accedi")
                            .fontWeight(.semibold)
                            .underline()
                            .foregroundStyle(.white)
                    }
                        
                    
                }
                .font(.caption)
            }
            .onAppear {
                Auth.auth().addStateDidChangeListener { auth, user in
                    if user != nil {
                        userIsLoggedIn.toggle()
                    }
                }
            }
            
            //Profile option
            VStack(spacing:24) {
                ProfileOptionRowView(imageName: "gear", title: "Settings")
                
                ProfileOptionRowView(imageName: "gear", title: "Accessibility")
                
                ProfileOptionRowView(imageName: "questionmark.circle", title: "visit the help center")
            }
            .padding(.vertical)
        }
        .padding()
    }
    
    func fetchCurrentUser() {
        if let user = Auth.auth().currentUser {
            self.currentUser = user.displayName
        } else {
            self.currentUser = nil
        }
            
    }
    
    func login() {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if error != nil {
                print(error!.localizedDescription)
            }
        }
    }
    
    func logout() {
        let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                // Opzionale: mostra un messaggio all'utente che il logout è avvenuto con successo
            } catch let signOutError as NSError {
                print("Errore durante il logout:", signOutError.localizedDescription)
                // Puoi gestire l'errore qui, ad esempio mostrando un messaggio all'utente
            }
    }
    
    func register() {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if let error = error {
                print("Errore durante la registrazione:", error.localizedDescription)
                return
            }
            
            // L'utente è stato registrato con successo
            if let user = result?.user {
                let changeRequest = user.createProfileChangeRequest()
                changeRequest.displayName = username // Imposta l'username
                changeRequest.commitChanges { error in
                    if let error = error {
                        print("Errore durante il salvataggio dell'username:", error.localizedDescription)
                        return
                    }
                    // Username salvato con successo
                    print("Username salvato con successo:", username)
                }
            }
        }
    }

}

#Preview {
    ProfileView()
}


extension View {
    func placeholder<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {
            
            ZStack(alignment: alignment){
                placeholder().opacity(shouldShow ? 1 : 0)
                self
            }
        }
}
