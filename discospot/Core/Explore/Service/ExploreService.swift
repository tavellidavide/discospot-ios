//
//  ExploreService.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import Foundation


class ExploreService {
    func fetchListings() async throws -> [Listing] {
        return DeveloperPreview.shared.listings
    }
}
