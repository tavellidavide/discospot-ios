//
//  EsploreView.swift
//  discospot
//
//  Created by Davide Tavelli on 09/04/24.
//

import SwiftUI

struct ExploreView: View {
    @State private var ShowDestinationSearchView = false
    @StateObject var viewModel = ExploreViewModel(service: ExploreService())
    
    var body: some View {
            NavigationStack{
                if ShowDestinationSearchView {
                    DestinationSearchView(show: $ShowDestinationSearchView, viewModel: viewModel)
                } else {
                    
                    VStack(spacing: -25) {
                        VStack(){
                            LinearGradient(gradient: Gradient(colors: [Color.red.opacity(0.2), Color.red.opacity(0.8)]), startPoint: .top, endPoint: .bottom)
                                .mask(Text("discospot").font(.custom("Druk Wide Bold", size: 45)))
                        }
                        .frame(height: 70)
                        

                        VStack(spacing: -50){
                            SearchAndFilterBar(location: $viewModel.searchLocation)
                                .onTapGesture {
                                    withAnimation(.snappy){
                                        ShowDestinationSearchView.toggle()
                                    }
                                }
                            
                        }
                        
                    }
                    ScrollView {
                        
                        LazyVStack() {
                            ForEach(viewModel.listings) { listing in
                                NavigationLink(value: listing) {
                                    ListingItemView(listing: listing)
                                        .frame(height: 400)
                                        .padding(.top, 15)
                                        .clipShape(RoundedRectangle(cornerRadius: 10))
                                }
                            }
                        }
                    }
                    .navigationDestination(for: Listing.self) { listing in
                        ListingDetailView(listing: listing)
                            .navigationBarBackButtonHidden()
                    }
                }
            }
    }
}

struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}







/*
 
 

 
 
 //
 //  EsploreView.swift
 //  discospot
 //
 //  Created by Davide Tavelli on 09/04/24.
 //

 import SwiftUI

 struct ExploreView: View {
     @State private var ShowDestinationSearchView = false
     @StateObject var viewModel = ExploreViewModel(service: ExploreService())
     
     var body: some View {
             NavigationStack{
                 if ShowDestinationSearchView {
                     DestinationSearchView(show: $ShowDestinationSearchView)
                 } else {
                     
                     VStack {
                         VStack{
                             Text("discospot")
                                 .font(Font.custom("Druk Wide Bold", size: 45))
                                 .foregroundStyle(.black)
                                 .background(.red)
                         }
                         .padding(.bottom, -8.5)

                     
                         VStack{
                             SearchAndFilterBar()
                                 .onTapGesture {
                                     withAnimation(.snappy){
                                         ShowDestinationSearchView.toggle()
                                     }
                                 }
                         }
                         .padding(.bottom, -20)
                         
                     }
                     .background()
                     .padding(.bottom, -5)
                     
                     ScrollView {
                         
                         LazyVStack(spacing: 32) {
                             ForEach(viewModel.listings) { listing in
                                 NavigationLink(value: listing) {
                                     ListingItemView(listing: listing)
                                         .frame(height: 400)
                                         .clipShape(RoundedRectangle(cornerRadius: 10))
                                 }
                             }
                         }
                         .padding()
                     }
                     .navigationDestination(for: Listing.self) { listing in
                         ListingDetailView(listing: listing)
                             .navigationBarBackButtonHidden()
                     }
                 }
             }
     }
 }

 struct ExploreView_Previews: PreviewProvider {
     static var previews: some View {
         ExploreView()
     }
 }




 
 
 
 
 
 */
