//
//  SearchAndFilterBar.swift
//  discospot
//
//  Created by Davide Tavelli on 10/04/24.
//

import SwiftUI

struct SearchAndFilterBar: View {

    @Binding var location: String
    var body: some View {
        HStack (spacing: 20){
            Image("ds_vector")
                .resizable()
                 .aspectRatio(contentMode: .fill)
                .frame(width: 32, height: 32, alignment: .topLeading)
                .colorInvert()
                .frame(minWidth: 32, minHeight: 32)

            
            VStack(alignment: .leading, spacing: 2) {
                Text(location.isEmpty ? "Trovami un party 🎉" : location)
                    .font(.footnote)
                    .fontWeight(.semibold)
                    .shadow(color: Color.red, radius: 5)
                
                Text("\(location.isEmpty ? "Ovunque - " : "")Qualsiasi giorno - Chiunque")
                    .font(.caption2)
                    .foregroundStyle(.white)
            }
            
                Spacer()
                
                Button(action: {}, label: {
                    Image(systemName: "line.3.horizontal.decrease.circle")
                        .foregroundStyle(.white)
                })
        }
        .padding(.horizontal)
        .padding(.vertical, 10)
        .overlay{
            Capsule()
                .stroke(lineWidth: 0.3)
                .foregroundStyle(Color(.gray))
        }
        .padding()
    }
}

struct SearchAndFilterBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchAndFilterBar(location: .constant("Los Angeles"))
    }
}
