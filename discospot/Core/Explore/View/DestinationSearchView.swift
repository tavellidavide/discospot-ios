//
//  DestinationSearchView.swift
//  discospot
//
//  Created by Davide Tavelli on 12/04/24.
//

import SwiftUI

enum DestinationSearchOptions {
    case location
    case dates
    case guests
}

struct DestinationSearchView: View {
    @Binding var show: Bool
    @ObservedObject var viewModel: ExploreViewModel
    @State private var selectedOption: DestinationSearchOptions = .location
    @State private var startDate = Date()
    @State private var endDate = Date()
    @State private var numGuests  = 0
    
    
    
    
    var body: some View {
        VStack(spacing: 20){
            HStack {
                Button {
                    withAnimation(.snappy) {
                        viewModel.updateListingsForLocation()
                        show.toggle()
                        
                    }

                } label: {
                    Image(systemName: "xmark.circle")
                        .imageScale(.large)
                        .foregroundStyle(.white)
                }
                
                Spacer()
                
                if !viewModel.searchLocation.isEmpty {
                    Button("Cancella") {
                        viewModel.searchLocation = ""
                        viewModel.updateListingsForLocation()
                    }
                    .foregroundStyle(.white)
                    .font(.subheadline)
                    .fontWeight(.semibold)
                }
            }
            .padding()
            
            
            VStack(alignment: .leading) {
                if selectedOption == .location {
                    Text("Dove vuoi andare?")
                        .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                        .fontWeight(.semibold)
                        .foregroundStyle(.white)
                    
                    HStack{
                        Image(systemName: "magnifyingglass")
                            .imageScale(.small)
                            .foregroundStyle(.white)
                        
                        TextField("Cerca la destinazione", text: $viewModel.searchLocation)
                            .font(.subheadline)
                            .foregroundStyle(.white)
                            .onSubmit {
                                viewModel.updateListingsForLocation()
                                show.toggle()
                            }
                    }
                    .frame(height: 44)
                    .padding(.horizontal)
                    .overlay {
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(lineWidth: /*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
                            .foregroundStyle(Color(.systemGray4))
                    }
                    
                } else {
                    CollapsedPickerView(title: "Where", description: "Add destination")
                    
                }
                
            }
            .modifier(CollapsibleDestinationViewModifier())
            .frame(height: selectedOption == .location ? 120 : 64)
            .onTapGesture {
                withAnimation(.snappy) {selectedOption = .location}
            }
            
            
            // date selection view
            VStack(alignment: .leading){
                if selectedOption == .dates {
                        Text("Quando vuoi festeggiare?")
                            .font(.title2)
                            .fontWeight(.semibold)
                        
                        VStack {
                            DatePicker("Da", selection: $startDate, displayedComponents: .date)
                            
                            Divider()
                            
                            DatePicker("A", selection: $endDate, displayedComponents: .date)
                        }
                        .foregroundColor(.gray)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                } else {
                    CollapsedPickerView(title: "Quando", description: "Aggiungi date")
                }
            }
            .modifier(CollapsibleDestinationViewModifier())
            .frame(height: selectedOption == .dates ? 180 : 64)
            .onTapGesture {
                withAnimation(.snappy) {selectedOption = .dates}
            }
            
            VStack(alignment: .leading){
                if selectedOption == .guests {
                    Text("Dove")
                        .font(.title2)
                        .fontWeight(.semibold)
                        
                    Stepper {
                        Text("\(numGuests) persone")
                    } onIncrement: {
                        numGuests += 1
                    } onDecrement: {
                        guard numGuests > 0 else {return}
                        numGuests -= 1
                    }
                } else {
                    CollapsedPickerView(title: "Chi", description: "Aggiungi ospiti")
                }
            }
            .modifier(CollapsibleDestinationViewModifier())
            .frame(height: selectedOption == .guests ? 120 : 64)
            .onTapGesture {
                withAnimation(.snappy) {selectedOption = .guests}
            }
            
            
            Spacer()
        }
    }
}

#Preview {
    DestinationSearchView(show: .constant(false), viewModel: ExploreViewModel(service: ExploreService()))
}

struct CollapsibleDestinationViewModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(.black)
            .clipShape(RoundedRectangle(cornerRadius: 12))
            .padding()
            .shadow(color: Color.red, radius: 7)

    }
}


struct CollapsedPickerView: View {
    let title: String
    let description: String
    
    var body: some View {
        VStack{
            HStack{
                Text(title)
                    .foregroundStyle(.gray)
                
                Spacer()
                
                Text(description)
            }
            .fontWeight(.semibold)
            .font(.subheadline)
        }
    }
}
