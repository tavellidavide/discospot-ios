//
//  ExploreViewModel.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import Foundation

class ExploreViewModel: ObservableObject {
    @Published var listings = [Listing]() // 2
    @Published var searchLocation = ""
    private let service: ExploreService
    private var listingsCopy = [Listing]() // 5
    
    init(service: ExploreService) {
        self.service = service
        
        Task {await fetchListings()}
    }
    
    func fetchListings() async {
        do {
            self.listings = try await service.fetchListings()
            self.listingsCopy = listings
        } catch {
            print("DEBUG: Failed to fetch listing with error: \(error.localizedDescription)")
        }
    }
    
    func updateListingsForLocation() {
        let filteredListings = listings.filter({
            $0.city.lowercased() == searchLocation.lowercased() ||
            $0.state.lowercased() == searchLocation.lowercased()
        })
        
        self.listings = filteredListings.isEmpty ? listingsCopy : filteredListings
    }
}
