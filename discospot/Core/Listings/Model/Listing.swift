//
//  Listing.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import Foundation

struct Listing: Identifiable, Codable, Hashable {
    let id: String
    let ownerUid: String
    let ownerName: String
    let ownerImageUrl: String
    let DescriptionProfile: String
    let partecipanti: Int
    var priceOfTheEvent: Int
    let latitude: Double
    let longitude: Double
    var imageURLs: [String]
    let address: String
    let city: String
    let state: String
    let title: String
    var rating: Double
    let features: [ListingFeatures]
    let amenities: [ListingAmenities]
    let type: ListingType
}

enum ListingFeatures: Int, Codable, Identifiable, Hashable {
    case atHouse
    case superHost
    
    var imageName: String {
        switch self {
        case .atHouse: return "door.left.hand.open"
        case .superHost: return "medal"
        }
    }
    
    var title: String {
        switch self {
        case .atHouse: return "Pagamento all'ingresso"
        case .superHost: return "Discospot SuperHost"
        }
    }
    
    var subtitle: String {
        switch self {
        case .atHouse: return "Per entrare all'evento è necessario pagare all'ingresso. "
        case .superHost: return "Evento organizzato da un SuperHost di DiscoSpot"
        }
    }
    
    var id: Int { return self.rawValue }
}

enum ListingAmenities: Int, Codable, Identifiable, Hashable {
    case pool
    case openbar
    case wifi
    case dj
    case tv
    case office
    
    var title: String {
        switch self {
        case .pool: return "Pool"
        case .openbar: return "Open Bar"
        case .wifi: return "Wi-fi"
        case .dj: return "DJ Set"
        case .tv: return "TV"
        case .office: return "Office"
        }
    }
    
    var imageName: String {
        switch self {
        case .pool: return "figure.pool.swim"
        case .openbar: return "wineglass"
        case .wifi: return "wifi.square"
        case .dj: return "music.note"
        case .tv: return "tv"
        case .office: return "pencil.and.ruler.fill"
        }
    }
    
    var id: Int { return self.rawValue }
}



enum ListingType: Int, Codable, Identifiable, Hashable {
    case ritrovo
    case raduno
    case evento
    case rave
    case party
    case pijama
    
    var typeofevent: String {
        switch self {
        case .ritrovo: return "Ritrovo"
        case .raduno: return "Raduno"
        case .evento: return "Evento"
        case .rave: return "Rave"
        case .party: return "Party"
        case .pijama: return "Pijama Party"
        }
    }
    
    var id: Int { return self.rawValue}
}

