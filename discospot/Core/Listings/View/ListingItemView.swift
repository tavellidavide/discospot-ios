//
//  ListingView.swift
//  discospot
//
//  Created by Davide Tavelli on 09/04/24.
//

import SwiftUI

struct ListingItemView: View {
    let listing: Listing
    var body: some View {
        VStack(spacing: 11) {
            // images
            ListingImageCarouselView(listing: listing)
                .frame(height: 320)
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .shadow(color: Color.gray, radius: 5)
            //listing details
            
            HStack(alignment: .top){
                
                // details
                VStack(alignment: .leading) {
                    Text("\(listing.title)")
                        .fontWeight(.semibold)
                        .foregroundStyle(.white)
                    
                    
                    Text("\(listing.city), \(listing.state)")
                        .foregroundStyle(.gray)
                    
                    Text("Nov 3 - 10")
                        .foregroundStyle(.gray)
                    
                    HStack(spacing: 4){
                        
                        Text("\(listing.type.typeofevent) organizzato da \(listing.ownerName) ")
                            .fontWeight(.semibold)
                        
                    }
                    .foregroundStyle(.white)
                    
                }
                
                
                Spacer()
                
                
                //rating
                
                
                HStack(spacing: 2){
                    ForEach(0..<Int(listing.rating), id: \.self) { _ in
                        Image(systemName: "star.fill")
                            .shadow(color: Color.gray, radius: 5)
                    }
                    
                    
                    
                    
                }
                .foregroundStyle(.white)
            }
            .font(.footnote)
            
        }
    }

}

struct ListingItemView_Previews: PreviewProvider {
    static var previews: some View {
        ListingItemView(listing: DeveloperPreview.shared.listings[0])
    }
}
