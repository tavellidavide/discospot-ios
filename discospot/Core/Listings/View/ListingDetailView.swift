//
//  ListingDetailView.swift
//  discospot
//
//  Created by Davide Tavelli on 11/04/24.
//

import SwiftUI
import MapKit

struct ListingDetailView: View {
    
    let listing: Listing
    
    @Environment(\.dismiss) var dismiss
    @State private var cameraPosition: MapCameraPosition
    
    init(listing: Listing) {
        self.listing = listing
        
        let region = MKCoordinateRegion(
            center: listing.city == "Los Angeles" ? .losAngeles : .miami,
            span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            )
        self._cameraPosition = State(initialValue: .region(region))
    }
    
    
    var body: some View {
        ScrollView{
            ZStack(alignment: .topLeading) {
                ListingImageCarouselView(listing: listing)
                    .frame(height: 320)
                
                Button {
                    dismiss()
                }label : {
                    Image(systemName: "chevron.left")
                        .foregroundStyle(.white)
                        .background() {
                            Circle()
                                .fill(.black)
                                .frame(width: 32, height: 32)
                        }
                        .padding(32)
                }
            }
            
            VStack(alignment: .leading, spacing: 8) {
                Text(listing.title)
                    .font(.title)
                    .fontWeight(.semibold)
                
                VStack(alignment: .leading) {
                            HStack(spacing: 2) {
                                ForEach(0..<Int(listing.rating), id: \.self) { _ in
                                    Image(systemName: "star.fill")
                                }
                                Text(" | ")
                                    .fontWeight(.semibold)
                                
                                Text("\(listing.partecipanti) partecipanti")
                                    .underline()
                                    .fontWeight(.semibold)
                            }
                            .foregroundStyle(.white)
                    Spacer()
                    
                    Text("\(listing.city), \(listing.state)")
                }
                .font(.caption)
            }
            .padding(.leading)
            .frame(maxWidth: .infinity, alignment: .leading)
            
            Divider()
            
            //listing features
            
            VStack(alignment: .leading, spacing: 16) {
                ForEach(listing.features) { feature in
                    HStack(spacing: 12){
                        Image(systemName: feature.imageName)
                        
                        VStack(alignment: .leading) {
                            Text(feature.title)
                                .font(.footnote)
                                .fontWeight(.semibold)
                            
                            Text(feature.subtitle)
                                .font(.caption)
                                .foregroundStyle(.gray)
                        }
                    }
                }
            }
            .padding()
            
            Divider()
            
            
            // host info view
            HStack {
                VStack(alignment: .leading, spacing: 4) {
                    Text("\(listing.type.typeofevent) organizzato da \(listing.ownerName)")
                        .font(.headline)
                    //.frame(width: 250, alignment: .leading)
                    
                    HStack {
                        Text("\(listing.DescriptionProfile)")
//                        Text("\(listing.numberOfBedrooms) eventi organizzati -")
//                        Text("\(listing.numberOfBeds) letti - ")
//                        Text("\(listing.numberOfBathrooms) baths")
                    }
                    .font(.caption)
                }
                .frame(width: 300,alignment: .leading)
                
                Spacer()
                
                Image("\(listing.ownerImageUrl)")
                    .resizable()
                    .frame(width: 64, height: 64)
                    .clipShape(Circle())
                    
            }
            .padding()
            
            Divider()
            
            
            
            /*VStack(alignment: .leading, spacing: 16){
                Text("Stanze disponibili")
                    .font(.headline)
                //Correggi!!
                ScrollView(.horizontal, showsIndicators: false){
                    HStack(spacing: 16) {
                        ForEach(1 ... listing.priceOfTheEvent, id: \.self) { bedroom in
                            VStack {
                                Image(systemName: "person.and.background.dotted")
                                
                                Text("Stanza \(bedroom)")
                            }
                            .frame(width: 132, height: 100)
                            .overlay{
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(lineWidth: 1)
                                    .foregroundStyle(.gray)
                            }
                        }
                    }
                }
            }
            .padding()
            
            Divider() */
            
            //Listing enum
            VStack(spacing: 16) {
                Text("L'evento comprende...")
                    .font(.headline)
                
                ForEach(listing.amenities) { amenity in
                    HStack{
                        Image(systemName: amenity.imageName)
                            .frame(width: 32)
                        
                        Text(amenity.title)
                            .font(.footnote)
                        
                        Spacer()
                    }
                }
            }
            .padding()
            
            Divider()
            
            VStack(alignment: .leading, spacing: 16) {
                Text("📍Posizione dell'evento...")
                    .font(.headline)
                
                    Map(position: $cameraPosition)
                        .frame(height: 200)
                        .clipShape(RoundedRectangle(cornerRadius: 12))
                

            }
            .padding()
        }
        .toolbar(.hidden, for: .tabBar)
        .ignoresSafeArea()
        .padding(.bottom, 64)
        .overlay(alignment: .bottom) {
            VStack {
                Divider()
                    .padding(.bottom)
                

                HStack {
                    VStack(alignment: .leading) {
                        Text("\(listing.priceOfTheEvent) €")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                        
                        
                        Text("per Prevendita")
                            .font(.footnote)
                        
                        Text("Sabato 15 ottobre")
                            .font(.footnote)
                            .fontWeight(.semibold)
                            .underline()
                        
                    }
                    
                    Spacer()
                    
                    Button {
                        
                    } label: {
                        Text("Mettiti in lista")
                            .foregroundStyle(.white)
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .frame(width: 140, height: 40)
                            .background(.red)
                            .clipShape(RoundedRectangle(cornerRadius: 8))
                        
                    }
                    
                }
                .padding(.horizontal, 32)

                Divider()
            }
            .background(.black)
            
        }
        
    }
}

struct ListingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ListingDetailView(listing: DeveloperPreview.shared.listings[0])
    }
}
