//
//  MainTabView.swift
//  discospot
//
//  Created by Davide Tavelli on 15/04/24.
//

import SwiftUI

struct MainTabView: View {
    
    @State private var selectedTab = 1
    
    var body: some View {
        TabView(selection: $selectedTab) {
            WishlistView()
                .tag(0)
                .tabItem { Label("", systemImage: "star") }
            
            ExploreView()
                .tag(1)
                .tabItem {Label("", systemImage: "bonjour") }
            
            ProfileView()
                .tag(2)
                .tabItem { Label("", systemImage: "person") }
        }
        .accentColor(.red)
    }
}

#Preview {
    MainTabView()
}
